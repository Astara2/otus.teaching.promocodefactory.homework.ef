﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    /// <summary>
    /// Данные запроса на добавление/изменение сотрудника
    /// </summary>
    public class EmployeeRequest
    {
        /// <summary>
        /// Идентификатор сотрудника
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Имя сотрудника
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия сотрудника
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        ///  Адрес эл.почты
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Количество использованных промокодов
        /// </summary>
        public int AppliedPromocodesCount { get; set; }
    }
}