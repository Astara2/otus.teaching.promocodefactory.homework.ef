﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
      
        /// <summary>
        /// Получить данные сотрудника по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Role = new RoleItemResponse()
                {
                    Name = employee.Role.Name,
                    Description = employee.Role.Description
                },
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        /// <param name="request">Данные сотрудника</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<EmployeeResponse>> CreateEmployeeAsync(EmployeeRequest request)
        {
            if (request == null)
                return NotFound();

            var employee = new Employee
            {
                Id = request.Id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email
            };

            var result = _employeeRepository.CreateAsync(employee);
            await result;

            if (result.IsFaulted)
                return BadRequest();

            var employeeModel = new EmployeeResponse
            {
                Id = employee.Id,
                FullName = employee.FullName,
                Email = employee.Email
            };

            return employeeModel; //Ok();
        }
        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <param name="id">Идентфиикатор сотрудника</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<StatusCodeResult> DeleteEmployeeAsync(Guid id)
        {
            if (id == null)
                return NotFound();
            var result = _employeeRepository.DeleteAsync(id);
            await result;

            if (result.IsFaulted)
                return BadRequest();

            return Ok();
        }

        /// <summary>
        /// Изменить сотрудника
        /// </summary>
        /// <param name="request">Данные сотрудника</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<StatusCodeResult> UpdateEmployeeAsync(EmployeeRequest request)
        {
            if (request == null)
                return NotFound();

            var employee = new Employee
            {
                Id = request.Id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                AppliedPromocodesCount = request.AppliedPromocodesCount
            };

            var result = _employeeRepository.UpdateAsync(employee);
            await result;

            if (result.IsFaulted)
                return BadRequest();

            return Ok();
        }


    }
}