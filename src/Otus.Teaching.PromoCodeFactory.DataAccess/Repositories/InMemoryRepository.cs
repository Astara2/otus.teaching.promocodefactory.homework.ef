﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task CreateAsync(T entity)
        {
            Data = Data.Append(entity);
            return Task.FromResult(Data);
        }

        public Task UpdateAsync(T entity)
        {
            var dataList = Data.ToList();
            
            int index = dataList.IndexOf(dataList.Where(x => x.Id == entity.Id).FirstOrDefault());
            dataList[index] = entity;

            Data = dataList;
            return Task.FromResult(Data);
        }

        public Task DeleteAsync(Guid id)
        {
            var newList = Data.ToList();
            newList.Remove(newList.First(x => x.Id == id));

            Data = newList;
            return Task.FromResult(Data);
        }

    }
}